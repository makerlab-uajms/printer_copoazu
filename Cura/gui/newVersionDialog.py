__copyright__ = "Copyright (C) 2013 David Braam - Released under terms of the AGPLv3 License"

import wx
from Cura.gui import firmwareInstall
from Cura.util import version
from Cura.util import profile

class newVersionDialog(wx.Dialog):
	def __init__(self):
		super(newVersionDialog, self).__init__(None, title="Bienvenido al programa de impresion 3D Copoazu!")

		wx.EVT_CLOSE(self, self.OnClose)

		p = wx.Panel(self)
		self.panel = p
		s = wx.BoxSizer()
		self.SetSizer(s)
		s.Add(p, flag=wx.ALL, border=15)
		s = wx.BoxSizer(wx.VERTICAL)
		p.SetSizer(s)

		title = wx.StaticText(p, -1, 'Copoazu - ' + version.getVersion())
		title.SetFont(wx.Font(18, wx.SWISS, wx.NORMAL, wx.BOLD))
		s.Add(title, flag=wx.ALIGN_CENTRE|wx.EXPAND|wx.BOTTOM, border=5)
		s.Add(wx.StaticText(p, -1, 'Este software esta desarrollado para la impresora 3D Copoazu de Sawers.'))
		s.Add(wx.StaticLine(p), flag=wx.EXPAND|wx.TOP|wx.BOTTOM, border=10)
		s.Add(wx.StaticText(p, -1, 'Este software fue modificado en la Universidad Juan Misael Saracho - UAJMS'))
		s.Add(wx.StaticText(p, -1, 'Cuenta con una interfaz muy amigable y con opciones a medida. Considerando:'))
		s.Add(wx.StaticText(p, -1, '* La mecanica.'))
		s.Add(wx.StaticText(p, -1, '* El ambiente de impresion.'))
		s.Add(wx.StaticText(p, -1, '* Usuarios inexpertos para comfiguraciones avanzadas.'))
		s.Add(wx.StaticText(p, -1, '* Tipos de filamento.'))
		s.Add(wx.StaticText(p, -1, '* La necesidad de utilizar el mas reciente CuraEngine.'))
 

		
		button = wx.Button(p, -1, 'Ok')
		self.Bind(wx.EVT_BUTTON, self.OnOk, button)
		s.Add(button, flag=wx.TOP|wx.ALIGN_RIGHT, border=5)

		self.Fit()
		self.Centre()

	def OnFirmwareInstall(self, index):
		firmwareInstall.InstallFirmware(machineIndex=index)

	def OnOk(self, e):
		self.Close()

	def OnClose(self, e):
		self.Destroy()
